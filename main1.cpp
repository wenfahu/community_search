#include<iostream>
#include<vector>
#include<random>
#include<ctime>
#include<sys/time.h>
#include <pthread.h>
#include <math.h>


using namespace std;

#define NUM_THREADS 8

vector<int> x;
const int MAX_INT = 1000000000;
const int sample_num = 100000000;

void generate_data()
{
    std::default_random_engine generator (time(0));
    std::uniform_int_distribution<int> distribution(0, MAX_INT);

    for (int i = 0; i < sample_num; i++) {
        x.push_back(distribution(generator));
    }

    cout << "Generated uniform distribution samples between 0 and " << MAX_INT << endl;
    cout << "Number of samples is " << sample_num << endl;
}

void *test(void *t)
{
    int sum = 0;
    for (int i=0; i<x.size(); i++)
        sum += x[i];
}

int main(int argc, char *argv[])
{
    struct timeval tp_begin, tp_end, tp_scan;
    void *status;
    int rc;
    pthread_attr_t attr;
    pthread_t tids[NUM_THREADS];

    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    generate_data();
    for(int i = 0; i < NUM_THREADS; ++i)
    {
        //参数依次是：创建的线程id，线程参数，调用的函数，传入的函数参数
        int ret = pthread_create(&tids[i], NULL, test, NULL);
        if (ret != 0)
        {
            cout << "pthread_create error: error_code=" << ret << endl;
        }
    }

    pthread_attr_destroy(&attr);
    gettimeofday(&tp_begin, NULL);
    for(int i=0; i < NUM_THREADS; i++ ){
        rc = pthread_join(tids[i], &status);
        if (rc){
            cout << "Error:unable to join," << rc << endl;
            exit(-1);
        }
        cout << "Main: completed thread id :" << i ;
        cout << "  exiting with status :" << status << endl;
    }
    gettimeofday(&tp_scan, NULL);
    int elapse_time = tp_scan.tv_usec - tp_begin.tv_usec;
    elapse_time += 1000000*(tp_scan.tv_sec - tp_begin.tv_sec);
    int n = x.size();
//    cout << "Sum is " << sum << endl;
    cout<< "scan time: " << elapse_time << "usec" << endl;
    cout<< "Bandwidth is " << NUM_THREADS * sizeof(int) * 1.0 * n / elapse_time <<" MB/s" << endl;
    pthread_exit(NULL);
}
