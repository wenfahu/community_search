//
// Created by liliang on 17-12-19.
//
#include <fstream>
#include <map>
#include <queue>
#include <iostream>
#include "CST.h"
#include <limits.h>
#include <cmath>
#include <time.h>

using namespace std;

CST::CST(int v0, int k) : v0(v0), g(Graph()), k(k) {
    this->load_data();
}

int CST::getV0() const {
    return v0;
}

int CST::getK() const {
    return this->k;
}

void ignore_line(ifstream &fin) {
    string s;
    getline(fin, s);
    cout << s << endl;
}

void CST::load_data() {
    ifstream fin("data/DBLP_410005575/DBLP/dblp.ungraph.txt");
//    ifstream fin("data/test.txt");
    if (!fin.is_open()) {
        cout << "opend fail" << endl;
        return;
    }
    ignore_line(fin);
    ignore_line(fin);
    ignore_line(fin);
    ignore_line(fin);

    int v0, v1;
    clock_t t1 = clock();
    int temp = -1;
    int v_count = 0;
    int e_count = 0;
    while (!fin.eof()) {
        fin >> v0;
        fin >> v1;
        this->g.addNode(v0);
        this->g.addNode(v1);
        this->g.addEdge(v0, v1);
        if (v0 != temp) {
            temp = v0;
            v_count += 1;
        }
        e_count += 1;
    }
    cout << "real v:" << v_count << ", real e:" << e_count << endl;
    g.setNodes_num(v_count);
    g.setEdges_num(e_count);

    auto& node_map = g.getNodeMap();
    int max_degree = -1;
    int max_index = -1;
    for (auto it = node_map.begin(); it != node_map.end(); ++it) {
        if (g.getNodeDegree((*it).first) > max_degree) {
            max_index = (*it).first;
            max_degree = g.getNodeDegree((*it).first);
        }
    }
    cout << "max degree:" << max_degree << ", max_index:" << max_index << endl;
    cout << "g.size():" << g.size() << endl;
    cout << (clock() - t1) * 1.0 / CLOCKS_PER_SEC << "SECS loading data" << endl;
}

Graph &CST::getGraph() {
    return this->g;
}


int CST::upperbound() {
    int V = this->g.getNodes();
    int E = this->g.getEdges();
    double tmp = (double)(9+8*(E - V));
    double res = (1+sqrt(tmp))/2;
    return (int)res;
}

map<int, set<int>> &Graph::getNodeMap() {
    return this->node_map;
}

void Graph::addNode(int v) {
    if (node_map.find(v) == node_map.end()) {
        node_map.insert(make_pair(v, set<int>()));
    }
}

void Graph::addEdge(int v1, int v2) {
//    if (!contains(v1)) {
//        node_map.insert(make_pair(v1, set<int>()));
//    }
//    if (!contains(v2)) {
//        node_map.insert(make_pair(v2, set<int>()));
//    }
    node_map[v1].insert(v2);
    node_map[v2].insert(v1);
}

set<int> &Graph::getOutNodes(int v) {
    return node_map[v];
}

int Graph::getNodeDegree(int v) {
    if (contains(v)) {
        return node_map[v].size();
    } else {
        cout << "segmentations error" << endl;
        return -1;
    }
}

Graph::Graph() : node_map(map<int, set<int>>()),
                 degree_map(map<int, set<int>>()),
                 min_degree(0), nodes_num(0), edges_num(0) {

}

Graph::Graph(const Graph& g){
    node_map = g.node_map;
    degree_map = g.degree_map;
    min_degree=g.min_degree;
    nodes_num = g.nodes_num; 
    edges_num = g.edges_num;
}

int Graph::size() {
    return node_map.size();
}

bool Graph::isEdge(int v1, int v2) {
    return node_map[v1].find(v2) != node_map[v1].end();
}

bool Graph::contains(int v) {
    return node_map.find(v) != node_map.end();
}

/**
 * 只是删除单向的边，从v1-->v2
 * @param v1
 * @param v2
 */

void Graph::removeEdge(int v1, int v2) {
    node_map[v1].erase(v2);
}

void Graph::removeNode(int v) {
    for (auto it = node_map[v].begin(); it != node_map[v].end(); ++it) {
        removeEdge((*it), v);
    }
    node_map.erase(v);
}

void Graph::buildConnectedComponent(int v, int k) {
    int current_v;
    set<int> visited;
    visited.insert(v);
    queue<int> q;
    q.push(v);
    while (!q.empty()) {
        current_v = q.front();
        q.pop();
        const set<int>& outNodes = getOutNodes(current_v);
        for (auto it = outNodes.begin(); it != outNodes.end(); ++it) {
            if (visited.find(*it) == visited.end()) {
                visited.insert(*it);
                q.push(*it);
            }
        }
    }
    cout << "node_map_size:" << node_map.size() << endl;
    cout << "visited:" << visited.size() << endl;
    set<int> nodeset;
    getNodeSet(nodeset);
    for (auto it = nodeset.begin(); it != nodeset.end(); ++it) {
        if (visited.find((*it)) == visited.end()) {
            removeNode((*it));
        }
    }
}

void Graph::degreeFilteredQueue(queue<int> &q, int k) {
    auto &node_map = getNodeMap();
    for (auto it = node_map.begin(); it != node_map.end(); ++it) {
        if ((*it).second.size() < k) {
            q.push((*it).first);
        }
    }
}

Graph* Graph::globalMCore(int v0){
    int current_v;
    auto &node_map = getNodeMap();
    map<int, int> degrees;

    while(true){
        for(auto it = node_map.begin(); it != node_map.end(); ++it){
            int pt_deg = it->second.size();
            degrees.insert(pair<int, int>(it->first,
                        pt_deg));
        }
        auto min_deg = min_element(degrees.begin(), degrees.end(),
                [](decltype(degrees)::value_type& l, decltype(degrees)::value_type& r) -> bool { return l.second < r.second; });
        int min_node = min_deg->first;
        if(min_node == v0)
            break;
        this->removeNode(min_node);
    }
    return this;
}

Graph *Graph::globalKCore(int v0, int k) {
    int current_v;
    queue<int> q;
    degreeFilteredQueue(q, k);

    cout << "filtered q size is:" << q.size() << endl;
    set<int> visited;
    while (!q.empty()) {
        current_v = q.front();
        q.pop();
        const set<int>& outNodes = getOutNodes(current_v);
        for (auto it = outNodes.begin(); it != outNodes.end(); ++it) {
            removeEdge(*it, current_v);
            if (visited.find(*it) != visited.end()) {
                continue;
            }
            if (getNodeDegree(*it) < k) {
                q.push(*it);
                visited.insert(*it);
            }
        }
        removeNode(current_v);
        if (!contains(v0)) {
            cout << "[*] Error globalKCore does't include v0" << endl;
            return NULL;
        }
        //get connected components
    }
    if (!contains(v0)) {
        cout << "[*] Error globalKCore does't include v0" << endl;
        return NULL;
    }
    buildConnectedComponent(v0, k);
    return this;
}


void Graph::addNodeAndEdges(int v, const set<int> &outs) {
    //功能是从其他图结构更新当前新的图结构，需要更新min_index，以及delta，
    addNode(v);
    if (degree_map.size() == 0) {
        degree_map[0] = set<int>();
        degree_map[0].insert(v);
        min_degree = 0;
        return;
    }
    int current_v_degree = 0;
    int another_degree;
    for (auto it = outs.begin(); it != outs.end(); ++it) {
        if (node_map.find(*it) != node_map.end()) {
            addEdge(v, *it);
            //update the affected nodes' min_index and delta
            current_v_degree++;
            another_degree = getNodeDegree(*it);
            degree_map[another_degree - 1].erase(*it);
            if (degree_map[another_degree - 1].size() == 0) {
                degree_map.erase(another_degree - 1);
                if (another_degree - 1 == min_degree) {
                    min_degree = another_degree;
                }
            }
            if (degree_map.find(another_degree) == degree_map.end()) {
                degree_map[another_degree] = set<int>();
                degree_map[another_degree].insert(*it);
            } else {
                degree_map[another_degree].insert(*it);
            }
        }
    }
    //update the current node' min_index and delta
    if (degree_map.find(current_v_degree) == degree_map.end()) {
        if (current_v_degree < min_degree) {
            min_degree = current_v_degree;
        }
        degree_map[min_degree] = set<int>();
        degree_map[min_degree].insert(v);
    } else {
        degree_map[current_v_degree].insert(v);
    }
}

void Graph::getNodeDegree(int v){
    return node_map[v].size();
}

void Graph::getNodeSet(set<int>& r) {
    r.clear();
    for (auto it = node_map.begin(); it != node_map.end(); ++it) {
        r.insert((*it).first);
    }
}

int Graph::getDelta() const {
    return min_degree;
}

int Graph::li(set<int> &outs) {
    int count = 0;
    for (auto it = outs.begin(); it != outs.end(); ++it) {
        if (node_map.find(*it) != node_map.end()) {
            count += 1;
        }
    }
    return count;
}

int Graph::lg(set<int> &outs) {
    int count = 0;
    int min_delta = INT_MAX;
    const set<int> &min_degreenodes = degree_map[min_degree];
    int flag = 0;
    int join = 0;
    for (auto it = outs.begin(); it != outs.end(); ++it) {
        if (node_map.find(*it) != node_map.end()) {
            count++;
            if (getNodeDegree(*it) == min_degree) {
                join++;
                flag = 1;
            }
        }
    }
    if (flag && join == min_degreenodes.size()) {
        min_delta = min_degree + 1;
    }

    if (count < min_delta) {
        return count;
    }
    return min_delta;
}

void Graph::setNodeMap(const map<int, set<int>> &m) {
    node_map = map<int, set<int>>(m);
}

int Graph::getNodes() {
    return nodes_num;
}

int Graph::getEdges() {
    return edges_num;
}

void Graph::setNodes_num(int nodes_num) {
    Graph::nodes_num = nodes_num;
}

void Graph::setEdges_num(int edges_num) {
    Graph::edges_num = edges_num;
}
