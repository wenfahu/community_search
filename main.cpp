#include<iostream>
#include<vector>
#include<random>
#include <map>
#include <queue>
#include <set>
#include "CST.h"

using namespace std;
#define NUM_THREADS 8


Graph *naiveCST(CST *cst) {
    int k = cst->getK();
    if (k > cst->upperbound()) {
        return NULL;
    }
    Graph &g = cst->getGraph();
    //Define the queue and visited set and outNodeSet
    queue<int> q;
    q.push(cst->getV0());
    set<int> visited;
    visited.insert(cst->getV0());

    cout << "g.contains(v0)?:" << g.contains(cst->getV0()) << endl;
    if (!g.contains(cst->getV0())) {
        return NULL;
    }
    cout << "v0:" <<cst->getV0()<<", degree:" << g.getNodeDegree(cst->getV0()) << endl;
    int current_v;
    Graph *C = new Graph();
    int count=0;
    while (!q.empty()) {
        count += 1;
        if (count % 1000 == 0) {
            cout << "Now we are poping time:" << count << endl;
            cout << "Now the delta is:" << C->getDelta() << endl;
        }
        current_v = q.front();
        q.pop();
        //C<-C | {v}
        const set<int>& outNodes = g.getOutNodes(current_v);
        C->addNodeAndEdges(current_v, outNodes);

        if (C->getDelta() >= k) {
            return C;
        }

        for (auto it = outNodes.begin();
             it != outNodes.end(); ++it) {
            if (visited.find(*it) == visited.end()
                && g.getNodeDegree(*it) >= k) {
                q.push(*it);
                visited.insert(*it);
            }
        }
    }
    cout << "Candidate selected C->size():" << C->size() << endl;
    clock_t t1 = clock();
    Graph * r = C->globalKCore(cst->getV0(),cst->getK());
    cout << (clock() - t1) * 1.0 / CLOCKS_PER_SEC << " SECS globalKCore time" << endl;
    return r;
}

Graph *lg_CST(CST *cst) {
    int k = cst->getK();
    if (k > cst->upperbound()) {
        return NULL;
    }
    Graph &g = cst->getGraph();
    //Define the queue and visited set and outNodeSet
    priority_queue<pair<int,int>> pq;
    pq.push(make_pair(0,cst->getV0()));
    set<int> visited;
    visited.insert(cst->getV0());
    set<int> outNodes;
    set<int> outs;
    int f;
    cout << "g.contains(v0)?:" << g.contains(cst->getV0()) << endl;
    if (!g.contains(cst->getV0())) {
        return NULL;
    }
    cout << "v0:" <<cst->getV0()<<", degree:" << g.getNodeDegree(cst->getV0()) << endl;
    int current_v;
    Graph *C = new Graph();
    int count=0;
    while (!pq.empty()) {
        count += 1;
        if (count % 1000 == 0) {
            cout << "Now we are poping time:" << count << endl;
            cout << "Now the delta is:" << C->getDelta() << endl;
        }
        current_v = pq.top().second;
        pq.pop();
        //C<-C | {v}
        outNodes = g.getOutNodes(current_v);
        C->addNodeAndEdges(current_v, outNodes);

        if (C->getDelta() >= k) {
            return C;
        }

        for (auto it = outNodes.begin();
             it != outNodes.end(); ++it) {
            if (visited.find(*it) == visited.end()
                && g.getNodeDegree(*it) >= k) {
                auto &outs = C->getOutNodes(*it);
                f = C->lg(outs) - C->getDelta();
                pq.push(make_pair(f, *it));
                visited.insert(*it);
            }
        }
    }
    cout << "Candidate selected C->size():" << C->size() << endl;
    return C->globalKCore(cst->getV0(),cst->getK());
}

Graph *li_CST(CST *cst) {
    int k = cst->getK();
    if (k > cst->upperbound()) {
        return NULL;
    }
    Graph &g = cst->getGraph();
    //Define the queue and visited set and outNodeSet
    priority_queue<pair<int,int>> pq;
    pq.push(make_pair(0,cst->getV0()));
    set<int> visited;
    visited.insert(cst->getV0());
    set<int> outNodes;
    set<int> outs;
    int f;

    cout << "g.contains(v0)?:" << g.contains(cst->getV0()) << endl;
    if (!g.contains(cst->getV0())) {
        return NULL;
    }
    cout << "v0:" <<cst->getV0()<<", degree:" << g.getNodeDegree(cst->getV0()) << endl;
    int current_v;
    Graph *C = new Graph();
    int count=0;
    while (!pq.empty()) {
        count += 1;
        if (count % 1000 == 0) {
            cout << "Now we are poping time:" << count << endl;
            cout << "Now the delta is:" << C->getDelta() << endl;
        }
        current_v = pq.top().second;
        pq.pop();
        //C<-C | {v}
        outNodes = g.getOutNodes(current_v);
        C->addNodeAndEdges(current_v, outNodes);

        if (C->getDelta() >= k) {
            cout << "Delta >=k, and Candidate selected C->size():" << C->size() << endl;
            return C;
        }

        for (auto it = outNodes.begin();
             it != outNodes.end(); ++it) {
            if (visited.find(*it) == visited.end()
                && g.getNodeDegree(*it) >= k) {
                outs = g.getOutNodes(*it);
                f = C->li(outs);
                outs.clear();
                pq.push(make_pair(f, *it));
                visited.insert(*it);
            }
        }
    }
    cout << "Candidate selected C->size():" << C->size() << endl;
    return C->globalKCore(cst->getV0(),cst->getK());
}

Graph * random_query(int k, Graph *graph_for_copy) {
    Graph *r = new Graph();
    r->setNodeMap(graph_for_copy->getNodeMap());
}

int main(int argc, char *argv[]) {
    int v0, k;
    cout << "please input v0 and k:";
    cin >> v0;
    cin >> k;
    CST *cst = new CST(v0, k);
    clock_t t1 = clock();
    Graph *r = naiveCST(cst);
    if (r == NULL) {
        cout <<"No such community contains v0"<<endl;
    } else{
        cout << "r->size():" << r->size() << endl;
    }
    cout << (clock() - t1) * 1.0 / CLOCKS_PER_SEC << "SECS total running" << endl;
    return 0;
}
