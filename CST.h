#include <vector>
#include <set>
#include <map>

using namespace std;
#ifndef CODE_CST_H
#define CODE_CST_H

class Graph {
private:
    map<int, set<int>> node_map;
    int min_degree;
    map<int, set<int>> degree_map;
    int nodes_num;
    int edges_num;
public:
    void setNodes_num(int nodes_num);

    void setEdges_num(int edges_num);

public:
    int getDelta() const;

    Graph();//构造函数
    Graph(const Graph& g);//构造函数

    void addNode(int v);

    void addNodeAndEdges(int v, const set<int> &outs);
    void getNodeDegree(int v);

    void addEdge(int v1, int v2);

    map<int, set<int>> &getNodeMap();

    void setNodeMap(const map<int, set<int>>& m);

    int getNodes();

    int getEdges();

    set<int> &getOutNodes(int v);

    void getNodeSet(set<int>& s);

    Graph *globalKCore(int v0, int k);
    Graph* globalMCore(int v0);

    void removeEdge(int v1, int v2);

    void removeNode(int v);

    int getNodeDegree(int v);

    int size();

    bool isEdge(int v1, int v2);

    bool contains(int v);

    void buildConnectedComponent(int v, int k);

    void degreeFilteredQueue(queue<int> &q, int k);

    int li(set<int>& outs);

    int lg(set<int>& outs);
};

class CST {
private:
    int v0;
    int k;
    Graph g;

public:
    CST(int v0, int k);

    int getK() const;

    int getV0() const;

    void load_data();

    Graph &getGraph();

    int upperbound();

};

#endif //CODE_CST_H
