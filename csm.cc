#include "CST.h"
#include <algorithm>
#include <iostream>
#include <math.h>

using namespace std;

bool sortbysec(const pair<int,int> &a,
              const pair<int,int> &b)
{
    return (a.second > b.second);
}

Graph *naiveCST(CST *cst) {
    int k = cst->getK();
    if (k > cst->upperbound()) {
        return NULL;
    }
    Graph &g = cst->getGraph();
    //Define the queue and visited set and outNodeSet
    queue<int> q;
    q.push(cst->getV0());
    set<int> visited;
    visited.insert(cst->getV0());

    cout << "g.contains(v0)?:" << g.contains(cst->getV0()) << endl;
    if (!g.contains(cst->getV0())) {
        return NULL;
    }
    cout << "v0:" <<cst->getV0()<<", degree:" << g.getNodeDegree(cst->getV0()) << endl;
    int current_v;
    Graph *C = new Graph();
    int count=0;
    while (!q.empty()) {
        count += 1;
        if (count % 1000 == 0) {
            cout << "Now we are poping time:" << count << endl;
            cout << "Now the delta is:" << C->getDelta() << endl;
        }
        current_v = q.front();
        q.pop();
        //C<-C | {v}
        const set<int>& outNodes = g.getOutNodes(current_v);
        C->addNodeAndEdges(current_v, outNodes);

        if (C->getDelta() >= k) {
            return C;
        }

        for (auto it = outNodes.begin();
             it != outNodes.end(); ++it) {
            if (visited.find(*it) == visited.end()
                && g.getNodeDegree(*it) >= k) {
                q.push(*it);
                visited.insert(*it);
            }
        }
    }
    cout << "Candidate selected C->size():" << C->size() << endl;
    clock_t t1 = clock();
    // Graph * r = C->globalKCore(cst->getV0(),cst->getK());
    cout << (clock() - t1) * 1.0 / CLOCKS_PER_SEC << " SECS globalKCore time" << endl;
    return C;
}

Graph* naiveCSM(CST* cst, float gamma){
    Graph& g = cst->getGraph();
    set<int> to_visit;
    set<int> visited;
    int v0 = cst->getV0;
    int upper = cst->upperbound();
    int degree_v0 = g.getOutNodes(v0).size();
    visited.insert(v0);
    set<int> outNodes;
    int current_v;
    Graph* visited_graph = new Graph();
    visited.addNode(v0);
    Graph *H = new Graph();
    int s=0;
    outNodes = g.getOutNodes(v0);
    to_visit.insert(outNodes.begin(), outNodes.end());
    current_v = v0;
    while(!to_visit.empyt() && s <= exp(-gamma) * floor( (g.nodes_num - g.edges_num) / 
                (( H.getDelta() + 1)/2 -1) - &H.size()) ){

        vector<pair<int, int> > A_to_B;
        for(auto t: to_visit){
            int link_cnt = 0;
            for(auto f: visited){
                if g.isEdge(*t, *f):
                    link_cnt++;
            }
            A_to_B.push_back(make_pair(*t, link_cnt));
        }
        // select vertex with max link from to_visit to visted
        sort(A_to_B.begin(), A_to_B.end(), sortbysec);
        int selected_vertex = A_to_B[0].first;
        s ++;
        visited.insert(selected_vertex);
        to_visit.erase(selected_vertex);
        visited_graph->addNodeAndEdges(current_v, set([selected_vertex]) ); 
        if (visited_graph->getDelta() > H->getDelta()){
            H = new Graph(*visited_graph);
            s = 0;
            if (H.getDelta() = min(degree_v0, upper) )
                return H;
        }
        // Add v’s neighbors with degree larger than  (G[H]) into B;
        set<int> neighbors = g.getOutNodes(selected_vertex);
        for(auto it=neighbors.begin(); it!=neighbors.end(); it++){
            if(*it > H.getDelta()){
                to_visit.inser(*it);
            }
        }
    }
    int k = H.getDelta();
    CST* cst = new CST(v0, k);
    Graph* r = naiveCST(cst);
    Graph* m = r->globalMCore(v0);
    if (m == NULL) {
        cout <<"No such community contains v0"<<endl;
    } else{
        cout << "r->size():" << m->size() << endl;
    }

}

